class Comment < ActiveRecord::Base
  #Indicar que um objecto comment pertence a um post
  #desta forma he efectuado um ele de ligacao entre os dois modelos
  #O post esta no singular pois he uma relacao de um para muitos
  belongs_to :post
  #Adicionar validacao de inputs
  #A validacao deve ser efectuda no modelo
  validates_presence_of :post_id
  validates_presence_of :body
end
