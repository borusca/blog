class Post < ActiveRecord::Base
  #Adicionar uma relacao na tabela indicando
  #que um post possui varios comentarios
  #o objecto coment deve estar no plurar aqui devido ha relacao um para muitos

  #Adicionarmos as tagas dependent: :destroy para garantir que quando um post he
  #eliminado os seus comentarios tambem o sao.
  #representa o on_cascade_delete do sql
  has_many :comments, dependent: :destroy
  #A validacao dos conteudos deve ser feita no modelo
  validates_presence_of :title
  validates_presence_of :body
end

